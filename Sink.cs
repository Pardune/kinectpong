using System;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace KinectDataServer
{
    public class Sink
    {
        private Boolean _state = false;
        private Broadcaster _bc = null;
        private Thread _bcThread = null;
        public RegistrationHandler _rh { get; set; }
        private Thread _rhThread = null;
        private GameObject _BodyView {get; set;}
        //private delegate void LogMsgSentCallback(String s);

        public Sink()
        {
            Debug.Log("[" + DateTime.Now.ToString("HH:mm:ss") + "]: Server Class Initialized\n");
            _BodyView = GameObject.Find("BodyView");
            button1_Click(null, null);
        }

        public void Stop()
        {
            button1_Click(null, null);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (_bc == null){
                _bc = new Broadcaster(this);
            }
            if(_bcThread == null){
                _bcThread = new Thread(new ThreadStart(_bc.Loop));
                _bcThread.Name = "Broadcaster Thread";
                _bcThread.IsBackground = true;
            }
            if (_rh == null)
            {
                _rh = new RegistrationHandler(this);
                _rh.SetBodyView(_BodyView);
            }
            if(_rhThread == null)
            {
                _rhThread = new Thread(new ThreadStart(_rh.RunHandler));
                _rhThread.Name = "Registrationhandler Thread";
                _rhThread.IsBackground = true;
            }

            if (!_state)
            {

                _bcThread.Start();
                _rhThread.Start();
                //while (!_bcThread.IsAlive) { }

                Debug.Log("[" + DateTime.Now.ToString("HH:mm:ss") + "]: Thread started\n");
                _state = true;
                //button1.Text = "Stop";
                
            }
            else
            {
                
                _bc.StopThread();
                _rh.StopRegistrationHandler();
                Debug.Log("[" + DateTime.Now.ToString("HH:mm:ss") + "]: Thread terminated\n");
                _state = false;
                _bc = null;
                _bcThread = null;
                _rh = null;
                _rhThread = null;
               //button1.Text = "Start";
            }
        }

        //Log the passed message
        public void LogMsg(String s)
        {
            /*string log = "[" + DateTime.Now.ToString("HH:mm:ss") + "]: " + s + "\n";
            if (rtb1.InvokeRequired)
            {
                LogMsgSentCallback d = new LogMsgSentCallback(LogMsg);
                this.Invoke(d, s);
            }
            else
            {*/
                Debug.Log("[" + DateTime.Now.ToString("HH:mm:ss") + "]: " + s);
            //}

        }

        /*/Autoscroll the Log
        private void rtb1_TextChanged(object sender, EventArgs e)
        {
            rtb1.SelectionStart = rtb1.Text.Length;
            rtb1.ScrollToCaret();
        }*/

        public void SetBodyView(GameObject g)
        {
            _BodyView = g;
        }
    }


    public class Broadcaster
    {
        private Sink mainwindow = null;
        private volatile bool running;


        public Broadcaster(Sink f)
        {
            mainwindow = f;
        }

        /// <summary>
        /// Advertise that this is the server every 2 seconds
        /// </summary>
        public void Loop()
        {
            running = true;
            UdpClient cl = new UdpClient();
            IPEndPoint ipe = new IPEndPoint(IPAddress.Broadcast, 15000);
            byte[] msg = Encoding.Unicode.GetBytes("I Am Server");
            while (running)
            {
                cl.Send(msg, msg.Length, ipe);
                //mainwindow.LogMsg("Identification Sent");
                Thread.Sleep(2000);
            }
            cl.Close();
        }

        /// <summary>
        /// Stop the Advertising Thread
        /// </summary>
        public void StopThread()
        {
            running = false;
        }

    }
}
